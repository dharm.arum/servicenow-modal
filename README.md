Running the ServiceNow modal application
========================================

Below steps explains how to run the modal application locally.

Steps to run the servicenow modal application locally using git
---------------------------------------------------------------

> - Install git in your machine.
> - Assuming you have a directory called codebase in your machine, go to terminal and $cd /codebase. Now you are inside codebase directory.
> - $git clone https://gitlab.com/dharm.arum/servicenow-modal.git
> - $cd service-nowmodal
> - You should see the following files - tests/ package.json karma.conf.js app/ README.md
> - $cd app/
> - Now open the app/index.html in any web browser


Steps to run the application locally using a zip file
-----------------------------------------------------
> - Unzip the servicenow-modal.zip anywhere in your machine.
> - Open the app/index.html in any web browser

Steps to run karma test
---------------------------
> - Install Node.js
> - $cd /servicenow-modal
> - $npm install karma --save-dev
> - $npm install karma-jasmine karma-chrome-launcher --save-dev
> - $npm install -g karma-cli
> - $npm install jasmine-core —save-dev
> - $npm install karma-ng-html2js-preprocessor --save-dev
> - $npm test
> - Running above npm test command will run all the test cases of the application

Using the snModal angular directive inside your application
---------------------------------------------------

###Introduction
sn-modal is a reusable angular directive that allows front end engineers to implement modal windows inside ServiceNow platform.

###Configuration
sn-modal attribute has following additional configuration

```
    header-title = <(required) Title of the modal window>
    close-caption = <(required) Close button text in modal window>
    submit-caption = <(required) Submit button text in modal window>
    modal-template-path = <(required) template path that goes as modal window content. This is application specific and must be a valid template that angular would be able to compile>
    id = <(required) unique sn-modal id>
    sn-modal-body = <(required) model binded to controller and view. It should be any valid JSON object>
```

###Example

Using sn-modal attribute in a div

>	configure sn-modal as an attribute in a div:
	```
	<div sn-modal header-title="2 way binded modal window" close-caption="Close" submit-caption="Submit" modal-template-path="app/templates/modal-1-Template.html" id="sn-modal-1" sn-modal-body="snModalBody"></div>
	```
	app/templates/modal-1-Template.html looks like below, but it can be anything that front end engineer can create:
	```
	<div style="padding: 20px">
		<label for="uModalBodyText">Modify the text here</label>
		<input type="text" role="textbox" id="uModalBodyText" ng-model="**snModalBody.text**"/>
	</div>
	```
	snModalBody model in controller:
	```
	**$scope.snModalBody = {
		"text": "2 way binded text"
	};**
	```
