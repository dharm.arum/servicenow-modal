/**
 *  servicenow dynamic item modal controller
 *  @controller SNDynamicItemsModalController depends on SNModalService
 *  $scope must have snModalBody object, specific to view/controller
 *  snModalBody.items contains list of item, to be displayed in the modal window
 *  snModalBody.errorText contains an error to display in the modal window(validation)
 *  @author dharma<dharmendranarumugam@gmail.com>
 * */
angular.module("app")
.controller("SNDynamicItemsModalController", function($scope, SNModalService){
	/*
	snModalBody contains view specific attributes
	*/
	$scope.snModalBody = {
		"items": [],
		"errorText":""
	};
	//
	var i=1,
		MAX_ITEMS=5;//max number of items that can be added
	/*
	* private function to init the default items on the modal window
	*/
	function _initItems(){
		var items=[];
		i=1;
		while(i <= 2){
			items.push("Item - " + i);
			i++;
		};
		//
		return items;
	};
	/*
	Calls SNModalService open() to open a modal window,
	registers for reject / resolve callbacks to handle the response appropriately
	*/
	$scope.open = function(snModalId){
		$scope.snModalBody.items = _initItems();
		$scope.snModalBody.errorText = "";
		var modalPromise = SNModalService.open(snModalId),
			msg="";
		//
		modalPromise.then(function(){
			msg = "You clicked ok";
			alert(msg);
			console.log(msg);
		}, function(){
			msg = "You cancelled it";
			alert(msg);
			console.log(msg);
		});
		//
	};
	/*
	* Adds an item to items array.
	* Validates for MAX_ITEMS and updates the errorText attribute
	*/
	$scope.snModalBody.addItem = function(){
		if($scope.snModalBody.items.length < MAX_ITEMS ){// can add up to 5 items
			$scope.snModalBody.items.push("Item - " + i++);
		}else{
			$scope.snModalBody.errorText = "Maximum of " + MAX_ITEMS + " items can be added.";
		}
		
	};
	//
});