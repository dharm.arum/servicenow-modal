/**
 *  servicenow dynamic item modal controller
 *  @controller SNWarningModalController depends on SNModalService
 *  $scope must have snModalBody object, specific to view/controller
 *  @author dharma<dharmendranarumugam@gmail.com>
 * */
angular.module("app")
.controller("SNWarningModalController", function($scope, SNModalService){
	//
	$scope.snModalBody = {
		"warningText": "This update will overwrite any existing data"
	};
	/*
	Calls SNModalService open() to open a modal window,
	registers for reject / resolve callbacks to handle the response appropriately
	*/
	$scope.open = function(snModalId){
		var modalPromise = SNModalService.open(snModalId),
			msg="";
		//
		modalPromise.then(function(){
			msg="You agreed";
			alert(msg);
			console.log(msg);
		}, function(){
			msg = "You Disagreed";
			alert(msg);
			console.log(msg);
		});
		//
	};
	//
});