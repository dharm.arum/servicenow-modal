/**
 *  servicenow confirm modal controller
 *  @controller SNConfirmModalController depends on SNModalService
 *  $scope must have snModalBody object, specific to view/controller
 *  @author dharma<dharmendranarumugam@gmail.com>
 * */
angular.module("app")
.controller("SNConfirmModalController", function($scope, SNModalService){
	/*
	snModalBody contains view specific attributes
	*/
	$scope.snModalBody = {
		"questionText": "Are you sure you want to delete the record ?"
	};
	/*
	Calls SNModalService open() to open a modal window,
	registers for reject / resolve callbacks to handle the response appropriately
	*/
	$scope.open = function(snModalId){
		var modalPromise = SNModalService.open(snModalId),
			msg="";
		//
		modalPromise.then(function(){
			msg = "You clicked Yes";
			alert(msg);
			console.log(msg);
		}, function(){
			msg = "You clicked No";
			alert(msg);
			console.log(msg);
		});
		//
	};
	//
});