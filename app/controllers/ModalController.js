/**
 *  2 way binding servicenow modal controller
 *  @controller SNModalController depends on SNModalService
 *  $scope must have snModalBody object, specific to view/controller,
 *  @author dharma<dharmendranarumugam@gmail.com>
 * */
angular.module("app")
.controller("SNModalController", function($scope, SNModalService){
	/*
	snModalBody contains view specific attributes
	*/
	$scope.snModalBody = {
		"text": "2 way binded text"
	};
	/*
	Calls SNModalService open() to open a modal window,
	registers for reject / resolve callbacks to handle the response appropriately
	*/
	$scope.open = function(snModalId){
		var modalPromise = SNModalService.open(snModalId),
			msg="";
		//
		modalPromise.then(function(){
			msg = "Submit button clicked - " + $scope.snModalBody.text;
			alert(msg);
			console.log(msg);
		}, function(){
			msg="Close button clicked";
			alert(msg);
			console.log(msg);
		});
		//
		return modalPromise;
	};
	//
});