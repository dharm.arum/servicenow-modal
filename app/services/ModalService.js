/**
 *  servicenow SNModalService factory service
 *  @service SNModalService
 *  Contains modalsMap hash with list of snModal registered by the directive.
 *  Using the modal id, snModal.open method is called back to the directive to take necessary actions.
 *  @author dharma<dharmendranarumugam@gmail.com>
 * */
angular.module("app")
.factory("SNModalService", function($q){
	//
	var modalsMap = [];
	/**
	factory function must return an object in angular
	*/
	return{
		open: open,
		registerModal: registerModal,
		unRegisterModal: unRegisterModal
	};
	/**
	Register the snModal in to map, returns modalsMap hash
	*/
	function registerModal(snModal){
		modalsMap[snModal["id"]] = snModal;
		return modalsMap;
	};
	/**
	Unregister the snModal from map
	*/
	function unRegisterModal(snModal){
		delete modalsMap[snModal["id"]];
		return modalsMap;
	};
	/**
	Invokes snModal instance open()
	Returns a promise either to resolve / reject when ok/cancel button clicked
	*/
	function open(snModalId){
		var modalWindow = modalsMap[snModalId],
			deferredObj = $q.defer();
		if(modalWindow){
			modalWindow["deferred"] = deferredObj;
			modalWindow.open();
		};
		return deferredObj.promise;
	};
	//
});