/**
 *  servicenow snModal directive
 *  @directive snModal
 *  $scope must have snModalBody object, specific to view/controller.
 *  app/templates/modalMasterTemplate.html is used as a master template, using the templateUrl property
 *  modalMasterTemplate has modal-header, model-body and model-footer.
 *  View specific attribute modalTemplatePath, is then injected in to modal-body of the template during the post linking function
 *  @author dharma<dharmendranarumugam@gmail.com>
 * */
angular.module("app")
.directive("snModal", function(SNModalService, $templateRequest, $compile){
	var mandatoryAttributes=['modalTemplatePath', 'id'];
	//converts camel case to dash case
	function _camelCaseToDash(camelCaseName){
		var regEx = /[A-Z]/g,
			separator = separator || '-';
			return camelCaseName.replace(regEx, function(letter, pos) {
				return (pos ? separator : '') + letter.toLowerCase();
			});
	};
	//link post function invoked by framework
	function _linkPostFunction(scope, iElement, iAttrs, controller, transcludeFn){
		//validate the incoming mandatory attributes
		mandatoryAttributes.map(function(item, index){
			if(!iAttrs[item]){
				throw new Error(_camelCaseToDash(item) + " attribute is mandatory for sn-modal directive");
				return;
			}
		});
		//
		//by default hide the modal window
		iElement.addClass("modal hideModal").children().addClass("modal-content");
		//
		var modalBody = null,
			closeActions = [],
			submitButton = null,
			snModal = {};
		//done via $templateRequest, but other option is to set transclude:true in the directive
		//and use ng-transclude in the template
	    $templateRequest(iAttrs.modalTemplatePath).then(function(template) {
	    	//jqLite is not supporting find by class or id, hence using the JS query selector
	    	modalBody = angular.element(iElement[0].querySelector(".modal-body"));
	        modalBody.append($compile(template)(scope));
	    });
	    //
		function _closeModal(){
			iElement.removeClass("showModal").addClass("hideModal");
			snModal.deferred.reject("cancel clicked");
		};
		//
		function _openModal(){
			iElement.removeClass("hideModal").addClass("showModal");
		};
		//
		function _submitModal(){
			iElement.removeClass("showModal").addClass("hideModal");
			snModal.deferred.resolve("ok buton clicked");
		};
		//
		snModal = {
			"id": iAttrs["id"],
			"open": _openModal,
			"deferred": null // other solution is to use & in isolated scope object
		};
		//
		SNModalService.registerModal(snModal);
		//clicking on anywhere outside the modal window must close it implicitly
		iElement.on("click", function(event){
			var target = angular.element(event.target);
			if(target.hasClass("modal")){//only when clicked outside the modal, close the modal window
				_closeModal();
			};
		});
		//clicking on the close icon on top right or the close button, must close the modal window
		closeActions = [iElement[0].querySelector(".close"), iElement[0].querySelector(".btn-default")];
		closeActions.map(function(item, index){
			angular.element(item).on("click", function(event){
				_closeModal();
			});
		});
		//
		submitButton = angular.element(iElement[0].querySelector(".btn-primary"));
		submitButton.on("click", function(event){
			_submitModal();
		});
		//clean up the resources here
		iElement.on("$destroy", function(){
			SNModalService.unRegisterModal(snModal);
		});
	};
	//
	return{
		restrict: "A",
		templateUrl: function(tElement, tAttrs){
			return "app/templates/modalMasterTemplate.html";
		},
		scope:{//create isoloated scope for the directive
			modalHeaderTitle: '@headerTitle',
			modalCloseButtonCaption: '@closeCaption',
			modalSubmitButtonCaption: '@submitCaption',
			snModalBody: '=snModalBody'
		},
		link: {
			post: _linkPostFunction
		}
	};
});