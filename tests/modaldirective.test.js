describe('SN Modal Directive Tests', function() {
  var $compile,
      $rootScope;

  // Load the app module
  beforeEach(module('app'));
  //Load the dependent modalMasterTemplate pre loaded by karma conf file
  beforeEach(module('app/templates/modalMasterTemplate.html'));
  // Store references to $rootScope and $compile
  // so they are available to all tests in this describe block
  beforeEach(inject(function(_$compile_, _$rootScope_){
    $compile = _$compile_;
    $rootScope = _$rootScope_;
  }));
  //
  it('Check for directive header exists? and header title matches?', function() {
    // Compile a piece of HTML containing the directive
    var element = $compile("<div sn-modal header-title='Test Header title' close-caption='Close Test' submit-caption='Ok Test'></div>")($rootScope);
    // fire all the watches, so the scope expressions will be evaluated and link post function of directive is called
    $rootScope.$digest();
    // Check that the compiled element contains modal header class
    expect(element[0].querySelector(".modal-header")).toBeDefined();
    //verify whether the passed header title matches, similarly we can test for other elements....
    expect(angular.element(element[0].querySelector(".modal-header h4")).html().trim()).toBe("Test Header title");
  });
});