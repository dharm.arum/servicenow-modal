describe("SN ModalControllerTest", function(){
	beforeEach(module("app"));
	var $controller;
	beforeEach(inject(function(_$controller_){
		$controller = _$controller_;
	}));
	//
  	describe("open modal test", function () {
  		//
  		it("check if controller is created successfully and open function available", function(){
  			var snModalController,
  				$scope={
  					"snModalBody":{}
  				};
  			snModalController = $controller('SNModalController', { $scope: $scope });
  			expect($scope.open).toBeDefined();
  		});
  		//
		it('check if controller open() returns a promise', function () {
			var $scope = {},
				snModalController,
				openPromise;
			$scope.snModalBody = {
				"text": "Two way binding text test"
			};
			snModalController = $controller('SNModalController', { $scope: $scope });
			openPromise = $scope.open('sn-model-test');
			expect(openPromise).toBeDefined();
			expect(openPromise.then instanceof Function).toBe(true);
			//
		});
		//
	});
	//
});