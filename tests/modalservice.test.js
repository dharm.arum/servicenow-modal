describe("sn modal service test", function(){
	var modalService;
	//
	beforeEach(module("app"));
	//
	beforeEach(inject(function (_SNModalService_) {
		modalService = _SNModalService_;
	}));
	//
	it("Verify whether snModal is able to register to service?", function(){
		var snModal = {
			"id": 1
		},
		modalsHash;
		//
		modalsHash = modalService.registerModal(snModal);
		expect(modalsHash[1]).toBeDefined();//snModal registered successfully
		expect(modalsHash[1]["id"]).toBe(1);
	});
	//
	it("Verify whether snModal is able return the registered snModal back?", function(){
		var snModal = {
			"id": 2
		},
		modalsHash;
		//
		modalsHash = modalService.registerModal(snModal);
		expect(modalsHash[2]["id"]).toBe(2);
	});
	//
});